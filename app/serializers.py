from django.db import transaction
from rest_framework.serializers import (
    ModelSerializer, SerializerMethodField,
    ValidationError
)
from app.models import Product, Order, OrderDetail
from rest_framework.validators import UniqueTogetherValidator


class ProductDefaultSerializer(ModelSerializer):

    def validate_stock(self, value):
        if value < 0:
            raise ValidationError("The minimum stock is zero")
        return value

    class Meta:
        model = Product
        fields = '__all__'


class OrderDetailDefaultSerializer(ModelSerializer):

    class Meta:
        model = OrderDetail
        fields = ['id', 'product', 'cuantity']


class OrderDetailCreateSerializer(ModelSerializer):

    @transaction.atomic
    def create(self, validated_data):
        product = validated_data.get("product")
        if product.stock < validated_data.get("cuantity"):
            raise ValidationError({"details": "Insuficient product stock."})
        ser = ProductDefaultSerializer(
            instance=product,
            data={
                "stock": product.stock - validated_data.get("cuantity")
            }, partial=True
        )
        ser.is_valid(raise_exception=True)
        ser.save()
        return super().create(validated_data)

    # self.initial_data.get("your field name")
    @transaction.atomic
    def update(self, instance, validated_data):
        diff = instance.cuantity - validated_data.get("cuantity")
        if diff < 0:
            if instance.product.stock < abs(diff):
                raise ValidationError(
                    {"details": "Insuficient product stock."})
        ser = ProductDefaultSerializer(
            instance=instance.product,
            data={
                "stock": instance.product.stock + diff
            }, partial=True
        )
        ser.is_valid(raise_exception=True)
        ser.save()
        return super().update(instance, validated_data)

    class Meta:
        model = OrderDetail
        fields = '__all__'
        validators = [
            UniqueTogetherValidator(
                queryset=OrderDetail.objects.all(),
                fields=['product', 'order'],
                message="Product in order duplicate."
            )
        ]


class OrderDefaultSerializer(ModelSerializer):
    details = OrderDetailDefaultSerializer(many=True, write_only=True)

    @transaction.atomic
    def create(self, validated_data):
        details = validated_data.pop("details", None)
        if not details:
            raise ValidationError({"details": "Details order required."})
        instance = Order.objects.create(**validated_data)

        for detail in details:
            ser = OrderDetailCreateSerializer(data={
                "product": detail.get("product").pk,
                "cuantity": detail.get("cuantity"),
                "order": instance.pk
            })
            ser.is_valid(raise_exception=True)
            ser.save()
        return instance

    @transaction.atomic
    def update(self, instance, validated_data):
        for detail in validated_data.get("details"):
            inst = instance.details_set.get(product=detail.get("product"))
            ser = OrderDetailCreateSerializer(
                inst, data={
                    "cuantity": detail.get("cuantity")
                }, partial=True
            )
            ser.is_valid(raise_exception=True)
            ser.save()
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        return {
            **super().to_representation(instance),
            "total": instance.get_total(),
            "total_usd": instance.get_total_usd(),
            "details": OrderDetailDefaultSerializer(
                instance.details_set, many=True
            ).data
        }

    class Meta:
        model = Order
        fields = '__all__'
