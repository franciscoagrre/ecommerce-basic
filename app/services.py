import requests


def api_dolarsi():
    resp = requests.get(
        "https://www.dolarsi.com/api/api.php?type=valoresprincipales")
    return resp.json()


def get_dolar_blue():
    response = api_dolarsi()
    for data in response:
        if data["casa"].get("nombre") == "Dolar Blue":
            return float(data["casa"].get("venta").replace(',', '.'))
    return None
