from rest_framework.routers import DefaultRouter
from app.views import ProductViewSet, OrderViewSet
from django.urls import include, path

router = DefaultRouter()
router.register('products', ProductViewSet, basename="products")
router.register('orders', OrderViewSet, basename="orders")

urlpatterns = [
    path('', include(router.urls)),
]
