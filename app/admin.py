from django.contrib import admin
from app.models import *


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'stock', 'price']


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['date_time']


@admin.register(OrderDetail)
class OrderDetailAdmin(admin.ModelAdmin):
    list_display = ('order', 'product', 'cuantity')
