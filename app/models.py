from django.db.models import (
    Model, CharField, FloatField, IntegerField,
    DateTimeField, Q, UniqueConstraint, CASCADE,
    ForeignKey, PROTECT, QuerySet
)
from django.core.validators import MinValueValidator
from django.db import transaction
from django.db.models import Sum
from app.services import get_dolar_blue


class Product(Model):
    name = CharField(max_length=100)
    price = FloatField()
    stock = IntegerField()


class Order(Model):
    date_time = DateTimeField()

    def get_total(self):
        return self.details_set.aggregate(
            total=Sum('product__price'),
        )["total"]

    def get_total_usd(self):
        dolar_blue = get_dolar_blue()
        if not isinstance(dolar_blue, float):
            return None
        return float(f'{self.get_total() / dolar_blue:.2f}')


class OrderDetailQuerySet(QuerySet):

    def delete(self, *args, **kwargs):
        [instance.delete() for instance in self]
        super().delete(*args, **kwargs)


class OrderDetail(Model):
    objects = OrderDetailQuerySet.as_manager()
    cuantity = IntegerField(
        validators=[MinValueValidator(1)]
    )
    order = ForeignKey(
        Order, on_delete=PROTECT,
        related_name="details_set"
    )
    product = ForeignKey(
        Product, on_delete=PROTECT,
        related_name="in_details_set"
    )

    @transaction.atomic
    def delete(self, *args, **kwargs):
        self.product.stock += self.cuantity
        self.product.save(update_fields=["stock"])
        return super().delete(*args, **kwargs)

    class Meta:
        constraints = [
            UniqueConstraint(
                fields=['order', 'product'],
                name='uniq_prod_in_order'
            )
        ]
