from factory import django, Faker, Factory, RelatedFactory, SubFactory
from app.models import Order, OrderDetail, Product
from faker import Faker as FakerClass
from random import uniform, randrange

faker = FakerClass()


class ProductModelFactory(django.DjangoModelFactory):

    class Meta:
        model = Product

    name = faker.text(max_nb_chars=Product._meta.get_field("name").max_length)
    price = round(uniform(1, 2000), 2)
    stock = randrange(0, 200)


class ProductFactory:

    def __init__(self, *args, **kwargs):
        self.name = faker.text(
            max_nb_chars=Product._meta.get_field("name").max_length)
        self.price = round(uniform(1, 2000), 2)
        self.stock = randrange(0, 200)


class OrderModelFactory(django.DjangoModelFactory):

    class Meta:
        model = Order

    date_time = faker.date_time()


class OrderDetailModelFactory(django.DjangoModelFactory):

    class Meta:
        model = OrderDetail

    cuantity = randrange(0, 5)
    product = SubFactory(ProductModelFactory)
    order = SubFactory(OrderModelFactory)
    # product = RelatedFactory(
    #    ProductModelFactory,
    #    factory_related_name='orders'
    # )
    # order = RelatedFactory(
    #    OrderModelFactory,
    #    factory_related_name='details'
    # )


class OrderDetailFactory:

    def __init__(self, product, cuantity):
        self.cuantity = cuantity
        self.product = product


class OrderFactory:

    def __init__(self):
        self.date_time = faker.date_time()
