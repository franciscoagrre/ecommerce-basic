from rest_framework.test import APITestCase, APIClient
from app.test.factory import ProductFactory, ProductModelFactory
from django.urls import reverse
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import User
from app.serializers import ProductDefaultSerializer
from app.models import Product
from random import randrange


class ProductTestCase(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = APIClient()
        cls.product = ProductFactory()
        user = User.objects.create_user(
            username='francisco', email='francisco@clicoh.com', password='R3des'
        )
        response = cls.client.post(
            "/api/token/", {"username": "francisco", "password": "R3des"},
            format='json'
        )
        cls.token = response.data["access"]

    def setUp(self):
        self.prod_model = ProductModelFactory()
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')

    def test_create(self):
        resp = self.client.post("/api/products/",  self.product.__dict__)
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.product.id = resp.json().get("id")

    def test_update(self):
        resp = self.client.patch(
            f'/api/products/{self.prod_model.id}/',
            ProductFactory().__dict__
        )
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_stock_gte_zero(self):
        resp = self.client.patch(
            f'/api/products/{self.prod_model.id}/',
            {"stock": randrange(-20, -1)}
        )
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)

    def test_retrieve(self):
        resp = self.client.get(f'/api/products/{self.prod_model.id}/')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(
            ProductDefaultSerializer(resp.json()).data,
            resp.json()
        )

    def test_destroy(self):
        resp = self.client.delete(f'/api/products/{self.prod_model.id}/')
        self.assertEqual(resp.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(
            Product.objects.filter(pk=self.prod_model.id).exists()
        )

    def test_list(self):
        ProductModelFactory.create_batch(10)
        resp = self.client.get(f'/api/products/')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIsInstance(resp.json(), list)
        # 10 + object created in setUp
        self.assertEqual(len(resp.json()), 11)
