from rest_framework.test import APITestCase, APIClient
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import User
from app.test.factory import *
from rest_framework import status
from app.models import Product
import random


class OrderTestCase(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = APIClient()
        cls.order = OrderFactory().__dict__
        cls.prods_model_list = ProductModelFactory.create_batch(10)
        user = User.objects.create_user(
            username='francisco', email='francisco@clicoh.com', password='R3des'
        )
        response = cls.client.post(
            "/api/token/", {"username": "francisco", "password": "R3des"},
            format='json'
        )
        cls.token = response.data["access"]

    def setUp(self):
        self.prod_model = ProductModelFactory()
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')

    def test_create(self):
        self.order.update({
            "details": [
                OrderDetailFactory(product=p.id, cuantity=p.stock).__dict__
                for p in self.prods_model_list
            ]
        })
        resp = self.client.post(
            "/api/orders/", self.order, format='json',
        )
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertFalse(
            Product.objects.exclude(
                id=self.prod_model.id
            ).filter(stock__gt=0).exists()
        )

    def test_uniq_prod_in_order(self):
        order = OrderFactory().__dict__
        order.update({
            "details": [
                {"product": self.prod_model.id, "cuantity": 1},
                {"product": self.prod_model.id, "cuantity": 1}
            ]
        })
        resp = self.client.post("/api/orders/", self.order)
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)

    def test_order_cuantity_gt_zero(self):
        order = OrderFactory().__dict__
        order.update({
            "details": [
                {"product": self.prod_model.id, "cuantity": 0}
            ]
        })
        resp = self.client.post("/api/orders/", order, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)

    def test_decrease_stock_product(self):
        product = ProductModelFactory(stock=random.randrange(50, 100))
        order = OrderFactory().__dict__
        cuantity = random.randrange(0, 50)
        order.update({
            "details": [
                {"product": product.id, "cuantity": cuantity}
            ]
        })
        resp = self.client.post("/api/orders/", order, format='json')
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            product.stock - cuantity,
            Product.objects.get(id=product.id).stock
        )

    def test_increase_stock_product_on_delete(self):
        order = OrderModelFactory()
        order_detail = OrderDetailModelFactory(
            cuantity=random.randrange(2, 50),
            product__stock=randrange(50, 100)
        )
        order.details_set.add(order_detail)
        resp = self.client.delete(f'/api/orders/{order.id}/')
        self.assertEqual(resp.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            order_detail.product.stock + order_detail.cuantity,
            Product.objects.get(id=order_detail.product.id).stock)

    def test_increase_stock_product_on_update(self):
        order = OrderModelFactory()
        order_detail = OrderDetailModelFactory(
            cuantity=10,
            product__stock=20
        )
        order.details_set.add(order_detail)
        resp = self.client.patch(
            f'/api/orders/{order.id}/',
            {'details': [{
                'product': order_detail.product.id,
                'cuantity': 5
            }]}, format='json'
        )
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(Product.objects.get(
            id=order_detail.product.id).stock, 25
        )
