from rest_framework.viewsets import ModelViewSet
from app.models import Product, Order, OrderDetail
from app.serializers import (
    ProductDefaultSerializer, OrderDefaultSerializer,
    OrderDetailDefaultSerializer
)
from django.db import transaction
from django.db.models import ProtectedError
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from app.exceptions import DefaultViewException


class ProductViewSet(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductDefaultSerializer
    permission_classes = [IsAuthenticated]

    def perform_destroy(self, instance):
        try:
            instance.delete()
        except ProtectedError:
            raise DefaultViewException(
                detail="Cannot delete because they are referenced through protected foreign keys",
                code=status.HTTP_406_NOT_ACCEPTABLE
            )


class OrderViewSet(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderDefaultSerializer
    permission_classes = [IsAuthenticated]

    @transaction.atomic
    def perform_destroy(self, instance):
        instance.details_set.all().delete()
        instance.delete()
