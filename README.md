## ClicOH challenge - ecommerce basic

[API Docs - Public postman account](https://www.postman.com/red-crater-785281/workspace/clicho/request/4513266-1b16a348-7a8d-4d93-9865-afa7417d8cb1).

## Mejoras:

- Usar cache en request a dolarsi.

## Potencial bug:

- Mandar guantity negativo en order detail.
